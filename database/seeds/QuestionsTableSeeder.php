<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => '1', 'question' => 'question1', 'answer1' => '1', 'answer2' => '2', 'answer3' => '3', 'answer4' => '4', 'answer5' => '5'],
            ['id' => '2', 'question' => 'question2', 'answer1' => '1', 'answer2' => '2', 'answer3' => '3', 'answer4' => '4', 'answer5' => '5'],
            ['id' => '3', 'question' => 'question3', 'answer1' => '1', 'answer2' => '2', 'answer3' => '3', 'answer4' => '4', 'answer5' => '5'],
            ['id' => '4', 'question' => 'question4', 'answer1' => '1', 'answer2' => '2', 'answer3' => '3', 'answer4' => '4', 'answer5' => '5'],
            ['id' => '5', 'question' => 'question5', 'answer1' => '1', 'answer2' => '2', 'answer3' => '3', 'answer4' => '4', 'answer5' => '5'],

        ]);
    }
}
