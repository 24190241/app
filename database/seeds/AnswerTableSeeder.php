<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            ['id' => '1', 'answer' => 'Very Good'],
            ['id' => '2', 'answer' => 'Good'],
            ['id' => '3', 'answer' => 'OK'],
            ['id' => '4', 'answer' => 'Bad'],
            ['id' => '5', 'answer' => 'Very Bad'],
        ]);   
    }
}
