<?php

use Illuminate\Database\Seeder;

class QuestionnairesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questionnaires')->insert([
            ['id' => '1', 'user_id' => '1', 'title' => 'seeded questionnaire', 'description' => 'test description', 'published' => '1',
            'question1' => '1', 'question2' => '2', 'question3' => '3', 'question4' => '4', 'question5' => '5']
        ]);
    }
}
