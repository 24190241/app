<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('questionnaire_id')->unsigned();
            //$table->foreign('questionnaire_id')->references('id')->on('questionnaires');

            $table->integer('answer1');
            //$table->foreign('answer_id')->references('id')->on('answers');
            $table->integer('answer2');
            //$table->foreign('answer_id')->references('id')->on('answers');
            $table->integer('answer3')->nullable();
            //$table->foreign('answer_id')->references('id')->on('answers');
            $table->integer('answer4')->nullable();
            //$table->foreign('answer_id')->references('id')->on('answers');
            $table->integer('answer5')->nullable();
            //$table->foreign('answer_id')->references('id')->on('answers');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('responses');
    }
}
