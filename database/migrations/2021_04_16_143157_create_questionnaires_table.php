<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionnairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questionnaires', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            //$table->foreign('user_id')->references('id')->on('users');
            $table->string('title');
            $table->string('description')->nullable();
            $table->boolean('published')->default(1);
            $table->integer('question1');
            //$table->foreign('question1')->references('id')->on('questions');
            $table->integer('question2');
            //$table->foreign('question2')->references('id')->on('questions');
            $table->integer('question3')->nullable();
            //$table->foreign('question3')->references('id')->on('questions');
            $table->integer('question4')->nullable();
            //$table->foreign('question4')->references('id')->on('questions');
            $table->integer('question5')->nullable();
            //$table->foreign('question5')->references('id')->on('questions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questionnaires');
    }
}
