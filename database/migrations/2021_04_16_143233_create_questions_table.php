<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question')->unique();
            $table->integer('answer1');
            //$table->foreign('answer1')->references('id')->on('answers');
            $table->integer('answer2');
            //$table->foreign('answer2')->references('id')->on('answers');
            $table->integer('answer3')->nullable();
            //$table->foreign('answer3')->references('id')->on('answers');
            $table->integer('answer4')->nullable();
            //$table->foreign('answer4')->references('id')->on('answers');
            $table->integer('answer5')->nullable();
            //$table->foreign('answer5')->references('id')->on('answers');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
