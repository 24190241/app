<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landingPage');
});

Route::get('/thankyou', function () {
    return view('thankyou');
});


Route::get('/response/questionnaires', 'ResponseController@index');
Route::get('/questionnaire/{questionnaire}/respond', 'ResponseController@show');
Route::post('/questionnaire/{questionnaire}/respond', 'ResponseController@store');
Route::resource('/respond', 'ResponseController');

Route::group(['middleware' => ['web']], function() {
    Auth::routes();

    Route::resource('/home', 'HomeController');

    Route::resource('/questionnaire', 'QuestionnaireController');
    Route::get('/questionnaire/{id}/responses', 'QuestionnaireController@show');
    Route::get('questionnaire/toggle/{id}', ['as' => 'questionnaire/toggle', 'uses' => 'QuestionnaireController@toggleBoolean']);

    Route::resource('/question', 'QuestionController');

    Route::resource('/answer', 'AnswerController');

});

