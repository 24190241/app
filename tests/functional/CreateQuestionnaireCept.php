<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Create a new questionnaire');

$I->haveRecord('answers', [
    'id' => '100',
    'answer' => 'Yes',
]);

$I->haveRecord('answers', [
    'id' => '200',
    'answer' => 'No'
]);

$I->haveRecord('questions', [
    'id' => '100',
    'question' => 'Test question 1',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('questions', [
    'id' => '200',
    'question' => 'Test question 2',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);


$I->amOnPage('/login');
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->click('.login');
//then
$I->amOnPage('/home');
$I->click('.viewQuestionnaires');
//then
$I->amOnPage('/questionnaires');
$I->see('Questionnaires', 'h1');
//and
$I->click('Add New');

//then
$I->amOnPage('/questionnaire/create');
$I->see('New Questionnaire', 'h1');
//and
$I->submitForm('.new-questionnaire', [
    'title' => 'Test Questionnaire',
    'description' => 'this is a test questionnaire',
    'question1' => '1',
    'question2' => '2',
    'question3' => null,
    'question4' => null,
    'question5' => null
    ]);

//then
$I->amOnPage('/questionnaireHome');
$I->see('Questionnaires', 'h1');
$I->see('Test Questionnaire');