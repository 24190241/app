<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('add a new answer');

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);
$I->amOnPage('/login');
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->click('.login');
//then
$I->amOnPage('/home');
$I->click('View your questionnaires');

//then
$I->amOnPage('/questionnaire');
$I->see('Questionnaires', 'h1');
//and
$I->click('Add New');

//when
$I->amOnPage('/questionnaire/create');
$I->See('New Questionnaire', 'h1');
//then
$I->click('Add Question');

//then
$I->amOnPage('/question/create');
$I->see('New Question', 'h1');
//then
$I->click('Add Answers');

//then
$I->amOnPage('/answer/create');
$I->see('Add Answers', 'h1');
//then
$I->submitForm('.new-answer', [
    'answer' => 'Yes'
]);

//then
$I->amOnPage('/createAnswers');
$I->see('Add Answers', 'h1');
//then
$I->submitForm('.new-answer', [
    'answer' => 'No'
]);

//then
$I->amOnPage('/createAnswers');
$I->click('Back to question');
$I->amOnPage('/createQuestion');