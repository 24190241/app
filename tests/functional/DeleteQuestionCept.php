<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('Delete a question');

$I->haveRecord('answers', [
    'id' => '100',
    'answer' => 'Yes',
]);

$I->haveRecord('answers', [
    'id' => '200',
    'answer' => 'No'
]);

$I->haveRecord('questions', [
    'id' => '100',
    'question' => 'Test question 1',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('questions', [
    'id' => '200',
    'question' => 'Test question 2',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('questionnaires', [
    'id' => '100',
    'user_id' => '100',
    'title' => 'Test questionnaire',
    'description' => 'this is a test questionnaire',
    'question1' => '100',
    'question2' => '200',
    'question3' => null,
    'question4' => null,
    'question5' => null
]);

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);

$I->amOnPage('/login');
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->click('.login');
//then
$I->amOnPage('/home');
$I->click('viewQuestionnaires');

//when
$I->amOnPage('/questionnaire');
$I->see('Test Questionnaire');
$I->click('Edit');

//then
$I->amOnPage('/questionnaire/1/edit');
$I->see('Is this a Yes/No question?');
$I->click('Edit Question');

//then
$I->amOnPage('/question/1/edit');
$I->see('Edit Question', 'h1');
//and
$I->click('Delete Answer');

//then
$I->amOnPage('/questionnaire/1/edit');
$I->dontSee('Is this a Yes/No question?');