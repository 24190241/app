<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('log in');

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);

//when
$I->amOnPage('/');
//and
$I->click('Log In');

//then
$I->amOnPage('/login');
$I->see('Login', 'h1');
//and
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->click('Login');

//then
$I->seeCurrentURLEquals('/home');
$I->see('Home', 'h1');