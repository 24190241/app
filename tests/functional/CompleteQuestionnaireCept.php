<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('complete a questionnaire');

$I->haveRecord('answers', [
    'id' => '100',
    'answer' => 'Yes',
]);

$I->haveRecord('answers', [
    'id' => '200',
    'answer' => 'No'
]);

$I->haveRecord('questions', [
    'id' => '100',
    'question' => 'Test question 1',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('questions', [
    'id' => '200',
    'question' => 'Test question 2',
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);

$I->haveRecord('questionnaires', [
    'id' => '100',
    'user_id' => '100',
    'title' => 'Test questionnaire',
    'description' => 'this is a test questionnaire',
    'question1' => '100',
    'question2' => '200',
    'question3' => null,
    'question4' => null,
    'question5' => null
]);

//when
$I->amOnPage('/');
//then
$I->click('View questionnaires looking for responses');

//then
$I->amOnPage('/response/questionnaires');
$I->see('View questionnaires accepting responses', 'h1');
//and
$I->see('Test Questionnaire');
$I->click('Respond');

//then
$I->amOnPage('/questionnaire/1/respond');
$I->submitForm('.responseForm', [
    'answer1' => '100',
    'answer2' => '200',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);
//then
$I->amOnPage('/thankyou');
$I->see('Thank you for completing the questionnaire', 'h1');
$I->click('Return to the home page');