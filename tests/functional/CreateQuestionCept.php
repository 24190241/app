<?php 
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('add a new question');

$I->haveRecord('users', [
    'id' => '100',
    'name' => 'testUser1',
    'email' => 'test@user.com',
    'password' => 'password'
]);

$I->haveRecord('answers', [
    'id' => '100',
    'answer' => 'Yes',
]);

$I->haveRecord('answers', [
    'id' => '200',
    'answer' => 'No'
]);

$I->amOnPage('/login');
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->click('.login');
//then
$I->amOnPage('/home');
$I->click('.viewQuestionnaires');
//then
$I->amOnPage('/Questionnniare/create');
$I->See('New Questionnaire', 'h1');
//then
$I->click('Add Question');

//then
$I->amOnPage('/Questionn/create');
$I->see('New Question', 'h1');
//then
$I->submitForm('.createQuestion', [
    'question' => 'Is this a Yes/No question?',
    'answer1' => '1',
    'answer2' => '2',
    'answer3' => null,
    'answer4' => null,
    'answer5' => null
]);

//then
$I->amOnPage('/Questionn/create');
$I->see('New Question', 'h1');
//then
$I->click('Back to questionnaire');

//then 
$I->amOnPage('/Questionnniare/create');
$I->see('New Questionnaire', 'h1');