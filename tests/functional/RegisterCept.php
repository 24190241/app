<?php 
$I = new FunctionalTester($scenario);

$I->am('user');
$I->wantTo('register');

//when
$I->amOnPage('/');
//and
$I->click('Log In');

//then
$I->amOnPage('/login');
$I->see('Login', 'h1');
//and
$I->click('Register');

//then
$I->amOnPage('/register');
$I->see('Register', 'h1');
$I->fillField('name', 'Test User');
$I->fillField('email', 'test@user.com');
$I->fillField('password', 'password');
$I->fillField('password_confirmation', 'password');
$I->click('Register');

//then
$I->amOnPage('/home');
$I->see('Home', 'h1');