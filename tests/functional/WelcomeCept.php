<?php 
$I = new FunctionalTester($scenario);

$I->am('a admin');
$I->wantTo('test laravel working');

//when
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->See('landing page');