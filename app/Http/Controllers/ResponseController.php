<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Response;
use App\Questionnaire;
use App\Question;
use App\Answer;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questionnaires = Questionnaire::all();

        return view('admin/response/home', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = Response::create($request->all());

        return redirect('/thankyou');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //pulling the questionnaire data
        $questionnaire = Questionnaire::findOrFail($id);

        //pulling all of the data for question1
        $question1 = Question::find($questionnaire->question1);
        $question1answer1 = Answer::find($question1->answer1);
        $question1answer2 = Answer::find($question1->answer2);
        $question1answer3 = Answer::find($question1->answer3);
        $question1answer4 = Answer::find($question1->answer4);
        $question1answer5 = Answer::find($question1->answer5);

        $question1answers = [
            $question1answer0 = 'Select...',
            $question1answer1->answer,
            $question1answer2->answer,
            $question1answer3->answer,
            $question1answer4->answer,
            $question1answer5->answer
        ];
    
         //pulling all of the data for question2
        $question2 = Question::find($questionnaire->question2);
        $question2answer1 = Answer::find($question1->answer1);
        $question2answer2 = Answer::find($question2->answer2);
        $question2answer3 = Answer::find($question2->answer3);
        $question2answer4 = Answer::find($question2->answer4);
        $question2answer5 = Answer::find($question2->answer5);

        $question2answers = [
            $question2answer0 = 'Select...',
            $question2answer1->answer,
            $question2answer2->answer,
            $question2answer3->answer,
            $question2answer4->answer,
            $question2answer5->answer
        ];

        //pulling all of the data for question3
        $question3 = Question::find($questionnaire->question3);
        $question3answer1 = Answer::find($question3->answer1);
        $question3answer2 = Answer::find($question3->answer2);
        $question3answer3 = Answer::find($question3->answer3);
        $question3answer4 = Answer::find($question3->answer4);
        $question3answer5 = Answer::find($question3->answer5);
        
        $question3answers = [
            $question3answer0 = 'Select...',
            $question3answer1->answer,
            $question3answer2->answer,
            $question3answer3->answer,
            $question3answer4->answer,
            $question3answer5->answer
        ];

        //pulling all of the data for question4
        $question4 = Question::find($questionnaire->question4);
        $question4answer1 = Answer::find($question4->answer1);
        $question4answer2 = Answer::find($question4->answer2);
        $question4answer3 = Answer::find($question4->answer3);
        $question4answer4 = Answer::find($question4->answer4);
        $question4answer5 = Answer::find($question4->answer5);

        $question4answers = [
            $question4answer0 = 'Select...',
            $question4answer1->answer,
            $question4answer2->answer,
            $question4answer3->answer,
            $question4answer4->answer,
            $question4answer5->answer
        ];

        //pulling all of the data for question5
        $question5 = Question::find($questionnaire->question5);
        $question5answer1 = Answer::find($question5->answer1);
        $question5answer2 = Answer::find($question5->answer2);
        $question5answer3 = Answer::find($question5->answer3);
        $question5answer4 = Answer::find($question5->answer4);
        $question5answer5 = Answer::find($question5->answer5);

        $question5answers = [
            $question5answer0 = 'Select...',
            $question5answer1->answer,
            $question5answer2->answer,
            $question5answer3->answer,
            $question5answer4->answer,
            $question5answer5->answer
        ];

        //pushing the questionnaire data to the view and return the view to the user
        return view('admin.response.show', compact('questionnaire',
                                                    'question1', 'question1answers',
                                                    'question2', 'question2answers',
                                                    'question3', 'question3answers',
                                                    'question4', 'question4answers',
                                                    'question5', 'question5answers'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
