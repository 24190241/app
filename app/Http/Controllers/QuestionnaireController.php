<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Questionnaire;
use App\Question;
use App\User;
use App\Response;
use App\Answer;

class QuestionnaireController extends Controller
{
    /**
     * Securing the set of pages using auth
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        // get all of the questionnaires
        $questionnaires = Questionnaire::all();

        return view('admin/questionnaire/questionnaire', ['questionnaires' => $questionnaires]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // getting all of the questions from the questions table
        $quest = Question::pluck('question', 'id');


        //getting the ID of the logged in user
        $user = Auth::user();

        //$questionnaire->user_id = \Auth::user()->id;

        // returning the create questionnaire view with the question data
        return view('admin/questionnaire/create', compact('quest', 'user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //storing the new questionnaire data
        $questionnaire = Questionnaire::create($request->all());
      
        return redirect('questionnaire');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $responses = Response::all();

        $question1 = Question::find($questionnaire->question1);
        $question2 = Question::find($questionnaire->question2);
        $question3 = Question::find($questionnaire->question3);
        $question4 = Question::find($questionnaire->question4);
        $question5 = Question::find($questionnaire->question5);

        $question1answer1 = Answer::find($question1->answer1);
        $question1answer2 = Answer::find($question1->answer2);
        $question1answer3 = Answer::find($question1->answer3);
        $question1answer4 = Answer::find($question1->answer4);
        $question1answer5 = Answer::find($question1->answer5);

        $question2answer1 = Answer::find($question1->answer1);
        $question2answer2 = Answer::find($question2->answer2);
        $question2answer3 = Answer::find($question2->answer3);
        $question2answer4 = Answer::find($question2->answer4);
        $question2answer5 = Answer::find($question2->answer5);

        $question3answer1 = Answer::find($question3->answer1);
        $question3answer2 = Answer::find($question3->answer2);
        $question3answer3 = Answer::find($question3->answer3);
        $question3answer4 = Answer::find($question3->answer4);
        $question3answer5 = Answer::find($question3->answer5);

        $question4answer1 = Answer::find($question4->answer1);
        $question4answer2 = Answer::find($question4->answer2);
        $question4answer3 = Answer::find($question4->answer3);
        $question4answer4 = Answer::find($question4->answer4);
        $question4answer5 = Answer::find($question4->answer5);

        $question5answer1 = Answer::find($question5->answer1);
        $question5answer2 = Answer::find($question5->answer2);
        $question5answer3 = Answer::find($question5->answer3);
        $question5answer4 = Answer::find($question5->answer4);
        $question5answer5 = Answer::find($question5->answer5);

        return view('admin.questionnaire.show', compact('questionnaire', 'responses', 
                                                        'question1', 'question1answer1', 'question1answer2', 'question1answer3', 'question1answer4', 'question1answer5',
                                                        'question2', 'question2answer1', 'question2answer2', 'question2answer3', 'question2answer4', 'question2answer5',
                                                        'question3', 'question3answer1', 'question3answer2', 'question3answer3', 'question3answer4', 'question3answer5',
                                                        'question4', 'question4answer1', 'question4answer2', 'question4answer3', 'question4answer4', 'question4answer5',
                                                        'question5', 'question5answer1', 'question5answer2', 'question5answer3', 'question5answer4', 'question5answer5'
                                                        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);

        // getting all of the questions from the questions table
        $quest = Question::pluck('question', 'id');

        return view('admin/questionnaire/edit', compact('questionnaire', 'quest'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire->update($request->all());
        return redirect('/questionnaire');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questionnaire = Questionnaire::findOrFail($id);
        $questionnaire->delete();

        return redirect('/questionniare');
    }

    public function toggleBoolean($id)
    {
        //finding the questionnaire related to the id 
        $questionnaire = Questionnaire::findOrFail($id);
        //inverting the boolean value of published
        $questionnaire->published = !$questionnaire->published;
        //updating the database
        $questionnaire->update();

        return view('admin/questionnaire/questionnaire');
    }

    
}
