<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Questionnaire extends Model
{
    protected $fillable = [
        'user_id',
        'title', 
        'description',
        'question1',
        'question2',
        'question3',
        'question4',
        'question5'
    ];

    /**
     * get all of the questions linked to the questionnaire
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function question() 
    {
        return $this->hasMany('App\Question');
    }

    /**
     * get the user associated with the questionnaire
     * 
     * @return mixed
     */

    public function user() 
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
