<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'question',
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer5'
    ];

    /**
     * Get the answers associated with the question
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function answers()
    {
        return $this->hasMany('App\Answer');
    }  
    
    public function questionnaires() 
    {
        return $this->belongsToMany('App\Questionnaire');
    }
}
