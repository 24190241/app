<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
    protected $fillable = [
        'questionnaire_id',
        'answer1',
        'answer2',
        'answer3',
        'answer4',
        'answer5'
    ];

    public function questionnaires(){
        return $this->belongsTo('App\Questionnaire');
    }
    public function answers() {
        return $this->hasMany('App\Answer');
    }
}
