@extends('layouts.master')

@section('title', 'Edit Questionnaire')

@section('content') 

    <h1 class="title">Edit Questionnaire - {{ $questionnaire->title}}</h1>

    <div class="publish">
        @if ($questionnaire->published === 0)
            <p>Questionnaire is not published.</p>
            <button class=""><a href="{{ route('questionnaire/toggle', ['id' => $questionnaire->id]) }}">Publish</a></button>
        </form>
        @elseif ($questionnaire->published === 1)
                <p>Questionnaire is published</p>
                <button class="button"><a href="{{ route('questionnaire/toggle', ['id' => $questionnaire->id]) }}">Unpublish</a></button>
            </form>        
        @endif
    </div>

    {!! Form::model($questionnaire, ['class' => 'inputForm', 'method' => 'POST', 'url' => 'questionnaire/' . $questionnaire->id]) !!}
    {{method_field('PATCH')}}

    <div>
        {!! Form::label('title', 'Title: ') !!}
        {!! Form::text('title', null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('description', 'Description: ') !!}
        {!! Form::textarea('description', null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('question1', 'Question 1: ') !!}
        <button class="smallEditButton"><a href="/question/{{ $questionnaire->question1 }}/edit">Edit Question 1</a></button>
        {!! Form::select('question1', $quest, null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('question2', 'Question 2: ') !!}
        <button class="smallEditButton"><a href="/question/{{ $questionnaire->question2 }}/edit">Edit Question 2</a></button>
        {!! Form::select('question2', $quest, null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('question3', 'Question 3: ') !!}
        <button class="smallEditButton"><a href="/question/{{ $questionnaire->question3 }}/edit">Edit Question 3</a></button>
        {!! Form::select('question3', $quest, null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('question4', 'Question 4: ') !!}
        <button class="smallEditButton"><a href="/question/{{ $questionnaire->question4 }}/edit">Edit Question 4</a></button>
        {!! Form::select('question4', $quest, null, ['class' => 'input']) !!}
    </div>
    <div>
        {!! Form::label('question5', 'Question 5: ') !!}
        <button class="smallEditButton"><a href="/question/{{ $questionnaire->question5 }}/edit">Edit Question 5</a></button>
        {!! Form::select('question5', $quest, null, ['class' => 'input']) !!}
    </div>
    
    <div>
        {!! Form::submit('Update Questionnaire', ['class' => 'submitButton']) !!}
    </div>

    {!! Form::close() !!}


@endsection