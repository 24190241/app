@extends('layouts.master')

@section('title', 'Create Questionnaire')

@section('content') 
    <div>
        <h1 class="title">New Questionnaire</h1>
        <button class="newQuestion"><a href="/question/create">Add new question</a></button>
    </div>
    
    {!! Form::open(array('action' => 'QuestionnaireController@store', 'id' => 'new-questionnaire', 'class' => 'inputForm')) !!}
            @csrf 

        <div class="hidden">
            {!! Form::label('user_id', 'ID: ') !!}
            {!! Form::text('user_id', $user->id) !!}
        </div>
        <div>
            {!! Form::label('title', 'Title: ') !!}
            {!! Form::text('title', null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('description', 'Description: ') !!}
            {!! Form::textarea('description', null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('question1', 'Question 1: ') !!}
            {!! Form::select('question1', $quest, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('question2', 'Question 2: ') !!}
            {!! Form::select('question2', $quest, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('question3', 'Question 3: ') !!}
            {!! Form::select('question3', $quest, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('question4', 'Question 4: ') !!}
            {!! Form::select('question4', $quest, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('question5', 'Question 5: ') !!}
            {!! Form::select('question5', $quest, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        
        <div>
            {!! Form::submit('Create Questionnaire', ['class' => 'submitButton']) !!}
        </div>
    {!! Form::close() !!}

    
@endsection