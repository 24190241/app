@extends('layouts.master')

@section('title', 'View Responses')

@section('content') 

    <h1 class="tableTitle">{{ $questionnaire->title }}</h1>

    @foreach ($responses as $response)
        @if ($response->questionnaire_id === $questionnaire->id)
            <table class="table">
                <thead>
                    <tr>
                        <td>Question</td>
                        <td>Submitted Answer</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $question1->question }}</td>
                        @if ($response->answer1 === $question1answer1->id)
                            <td>{{ $question1answer1->answer }}</td>
                        @elseif ($response->answer1 === $question1answer2->id)
                            <td>{{ $question1answer2->answer }}
                        @elseif ($response->answer1 === $question1answer3->id)
                            <td>{{ $question1answer3->answer }}
                        @elseif ($response->answer1 === $question1answer4->id)
                            <td>{{ $question1answer4->answer }}
                        @elseif ($response->answer1 === $question1answer5->id)
                            <td>{{ $question1answer5->answer }}
                        @endif
                    </tr>
                    <tr>
                        <td>{{ $question2->question }}</td>
                        @if ($response->answer2 === $question1answer1->id)
                            <td>{{ $question1answer1->answer }}</td>
                        @elseif ($response->answer2 === $question1answer2->id)
                            <td>{{ $question1answer2->answer }}
                        @elseif ($response->answer2 === $question1answer3->id)
                            <td>{{ $question1answer3->answer }}
                        @elseif ($response->answer2 === $question1answer4->id)
                            <td>{{ $question1answer4->answer }}
                        @elseif ($response->answer2 === $question1answer5->id)
                            <td>{{ $question1answer5->answer }}
                        @endif
                    </tr>
                    <tr>
                        <td>{{ $question3->question }}</td>
                        @if ($response->answer3 === $question1answer1->id)
                            <td>{{ $question1answer1->answer }}</td>
                        @elseif ($response->answer3 === $question1answer2->id)
                            <td>{{ $question1answer2->answer }}
                        @elseif ($response->answer3 === $question1answer3->id)
                            <td>{{ $question1answer3->answer }}
                        @elseif ($response->answer3 === $question1answer4->id)
                            <td>{{ $question1answer4->answer }}
                        @elseif ($response->answer3 === $question1answer5->id)
                            <td>{{ $question1answer5->answer }}
                        @endif
                    </tr>
                    <tr>
                        <td>{{ $question4->question }}</td>
                        @if ($response->answer4 === $question1answer1->id)
                            <td>{{ $question1answer1->answer }}</td>
                        @elseif ($response->answer4 === $question1answer2->id)
                            <td>{{ $question1answer2->answer }}
                        @elseif ($response->answer4 === $question1answer3->id)
                            <td>{{ $question1answer3->answer }}
                        @elseif ($response->answer4 === $question1answer4->id)
                            <td>{{ $question1answer4->answer }}
                        @elseif ($response->answer4 === $question1answer5->id)
                            <td>{{ $question1answer5->answer }}
                        @endif
                    </tr>
                    <tr>
                        <td>{{ $question5->question }}</td>
                        @if ($response->answer5 === $question1answer1->id)
                            <td>{{ $question1answer1->answer }}</td>
                        @elseif ($response->answer5 === $question1answer2->id)
                            <td>{{ $question1answer2->answer }}
                        @elseif ($response->answer5 === $question1answer3->id)
                            <td>{{ $question1answer3->answer }}
                        @elseif ($response->answer5 === $question1answer4->id)
                            <td>{{ $question1answer4->answer }}
                        @elseif ($response->answer5 === $question1answer5->id)
                            <td>{{ $question1answer5->answer }}
                        @endif
                    </tr>
                </tbody>
            </table>
        @endif
    @endforeach
@endsection