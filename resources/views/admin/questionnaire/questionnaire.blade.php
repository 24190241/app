@extends('layouts.master')

@section('title', 'Questionnaire Home')

@section('content') 
    <h1 class="tableTitle">Questionnaires</h1>
    <div>
        <button class="newQuestionnaire"><a href="questionnaire/create">Add New</a></button>
    </div>

    <section>
        @if (isset ($questionnaires))

            <table class="table">
                <thead>
                    <tr>
                        <td>Questionnaire</td>
                        <td>Edit</td>
                        <td>Delete</td>
                        <td>Responses</td>
                    </tr>
                </thead>
                <tbody>
                @foreach ($questionnaires as $questionnaire)
                    <tr>
                        <td>{{ $questionnaire->title }}:
                            {{ $questionnaire->description }}</td>
                        <td><button class="editButton"><a href="questionnaire/{{ $questionnaire->id }}/edit">Edit</a></button></td>
                        <td>
                            {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaire.destroy', '{{ $questionnaire->id }}' ]]) !!}
                            {!! Form::submit('Delete', ['class' => 'deleteButton']) !!}
                            {!! Form::close() !!}
                        </td>
                        <td><button class="respondButton"><a href="questionnaire/{{ $questionnaire->id }}/responses">View Responses</a></button></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else 
            <p>No questionnaires addedd yet</p>
        @endif
    </section>
    
    
@endsection