@extends('layouts.master')

@section('title', 'Create Answer')

@section('content') 

    <div>
        <h1 class="title">New Answer</h1>
        <button class="backButton"><a href="/question/create">Back to question</a></button>
    </div>
    
    {!! Form::open(array('action' => 'AnswerController@store', 'id' => 'new-answer', 'class' => 'inputForm')) !!}
            @csrf
        <div> 
            {!! Form::label('answer', 'Answer:') !!}
            {!! Form::text('answer', null, ['class' => 'input']) !!}
        </div>

        <div>
            {!! Form::submit('Add Answer', ['class' => 'submitButton']) !!}
        </div>
    {!! Form::close() !!}
@endsection