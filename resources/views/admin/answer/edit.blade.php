@extends('layouts.master')

@section('title', 'Edit Answer')

@section('content')

<h1 class="title">Edit Answer - {{ $answer->answer }}</h1>
<p class="note">Note, If you edit this answer, it will affect every question it is linked to.</p>

    {!! Form::model($answer, ['class' => 'inputForm', 'method' => 'POST', 'url' => 'answer/' . $answer-> id]) !!}
    {{method_field('PATCH')}}

        <div> 
            {!! Form::label('answer', 'Answer:') !!}
            {!! Form::text('answer', null, ['class' => 'input']) !!}
        </div>

        <div>
            {!! Form::submit('Update Answer', ['class' => 'submitButton']) !!}
        </div>
        <button class="submitButton" action="answer.destroy" method="delete">Delete Answer</button>

    {!! Form::close() !!}

@endsection