<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Respond Page</title>
    <link rel="stylesheet" href="/css/app.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li class="left"><a href="/">Home</a></li>
                <li><a href="{{ route('login') }}">Log In</a></li>
            </ul>
        </nav>
    </header>
    <article>
        <h1 class=tableTitle>{{ $questionnaire->title }}</h1>
        <p class="description">{{ $questionnaire->description }}</p>

        {!! Form::open(array('action' => 'ResponseController@store', 'uri' => 'questionnaire/{{ $questionnaire->id }}/respond', 'method' => 'post', 'id' => '{questionnaire->id}', 'class' => 'responseForm')) !!}
            @csrf 

            <div class="hidden">
                {!! Form::label('questionnaire_id', 'ID: ') !!}
                {!! Form::text('questionnaire_id', $questionnaire->id) !!}
            </div>
            <div>
                {!! Form::label('answer1', $question1->question) !!}
                {!! Form::select('answer1', $question1answers, ['class' => 'responseInput', 'placeholder' => 'Select...']) !!}
            </div>
            <div>
                {!! Form::label('answer2', $question2->question) !!}
                {!! Form::select('answer2', $question2answers, ['class' => 'responseInput', 'placeholder' => 'Select...']) !!}
            </div>
            <div>
                {!! Form::label('answer3', $question3->question) !!}
                {!! Form::select('answer3', $question3answers, ['class' => 'responseInput', 'placeholder' => 'Select...']) !!}
            </div>
            <div>
                {!! Form::label('answer4', $question4->question) !!}
                {!! Form::select('answer4', $question4answers, ['class' => 'responseInput', 'placeholder' => 'Select...']) !!}
            </div>
            <div>
                {!! Form::label('answer5', $question5->question) !!}
                {!! Form::select('answer5', $question5answers, ['class' => 'responseInput', 'placeholder' => 'Select...']) !!}
            </div>
            <div>
                {!! Form::submit('Submit your responses', ['class' => 'submitResponse']) !!}
        {!! Form::close() !!}
    </article>
</body>