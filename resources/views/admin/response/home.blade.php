<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Available Questionnaires</title>
    <link rel="stylesheet" href="/css/app.css">
    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro&display=swap" rel="stylesheet">
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li class="left"><a href="/">Home</a></li>
                <li><a href="{{ route('login') }}">Log In</a></li>
            </ul>
        </nav>
    </header>
    <article>
        <h1 class="tableTitle">View questionnaires accepting responses</h1>
        <button class="newQuestionnaire"><a href="/">Back</a></button>

        <section>
        @if (isset ($questionnaires))

            <table class="table">
                <thead>
                    <tr>
                        <td>Questionnaire</td>
                        <td>Respond</td>
                    </tr>
                </thead>
                <tbody>
                @foreach ($questionnaires as $questionnaire)
                    <tr>
                        @if ($questionnaire->published == 1)
                            <td>{{ $questionnaire->title }}: 
                                {{ $questionnaire->description }}</td>
                            <td><button class="respondButton"><a href="/questionnaire/{{ $questionnaire->id }}/respond">Respond</a></button></td>
                        @endif
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <p>No questionnaires are accepting responses at the moment, Please check back later.</p>
        @endif
        </section>
    </article>
</body>
</html>