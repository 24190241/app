@extends('layouts.master')

@section('title', 'Create Question')

@section('content') 
    <div>
        <h1 class="title">New Question</h1>
        <button class="backButton"><a href="/questionnaire/create">Back to questionnaire</a></button>
        <button class="newAnswer"><a href="/answer/create">Add New Answer</a></button>
    </div>

    
    {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createQuestion', 'class' => 'inputForm')) !!}
            @csrf 
        <div>
            {!! Form::label('question', 'Question: ') !!}
            {!! Form::text('question', null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer1', 'Answer: ') !!}
            {!! Form::select('answer1', $ans, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('answer2', 'Answer: ') !!}
            {!! Form::select('answer2', $ans, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('answer3', 'Answer: ') !!}
            {!! Form::select('answer3', $ans, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('answer4', 'Answer: ') !!}
            {!! Form::select('answer4', $ans, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::label('answer5', 'Answer: ') !!}
            {!! Form::select('answer5', $ans, null, ['class' => 'input', 'placeholder' => 'Select...']) !!}
        </div>
        <div>
            {!! Form::submit('Add Question', ['class' => 'submitButton']) !!}
        </div>
    {!! Form::close() !!}
@endsection