@extends('layouts.master')

@section('title', 'Edit Question')

@section('content') 

<h1 class="title">Edit Question - {{ $question->question }}</h1>
<p class="note">Note, If you edit this question, it will affect every questionnaire it is linked to.</p>

    {!! Form::model($question, ['class' => 'inputForm', 'method' => 'POST', 'url' => 'question/' . $question->id]) !!}
    {{method_field('PATCH')}}

        <div>
            {!! Form::label('question', 'Question: ') !!}
            {!! Form::text('question', null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer1', 'Answer: ') !!}
            <button class="smallEditButton"><a href="/answer/{{ $question->answer1 }}/edit">Edit Answer 1</a></button>
            {!! Form::select('answer1', $ans, null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer2', 'Answer: ') !!}
            <button class="smallEditButton"><a href="/answer/{{ $question->answer2 }}/edit">Edit Answer 2</a></button>
            {!! Form::select('answer2', $ans, null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer3', 'Answer: ') !!}
            <button class="smallEditButton"><a href="/answer/{{ $question->answer3 }}/edit">Edit Answer 3</a></button>
            {!! Form::select('answer3', $ans, null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer4', 'Answer: ') !!}
            <button class="smallEditButton"><a href="/answer/{{ $question->answer4 }}/edit">Edit Answer 4</a></button>
            {!! Form::select('answer4', $ans, null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::label('answer5', 'Answer: ') !!}
            <button class="smallEditButton"><a href="/answer/{{ $question->answer5 }}/edit">Edit Answer 5</a></button>
            {!! Form::select('answer5', $ans, null, ['class' => 'input']) !!}
        </div>
        <div>
            {!! Form::submit('Update Question', ['class' => 'submitButton']) !!}
        </div>
        <button class="submitButton" action="question.destroy" method="delete">Delete Question</button>
    {!! Form::close() !!}

@endsection