<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Landing Page</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li class="left"><a href="/">Home</a></li>
                <li><a href="{{ route('login') }}">Log In</a></li>
            </ul>
        </nav>
    </header>
    <article>
            <div class="leftcontainer" style="float: left">
                <img src="/images/questionnaire.jpg" alt="image of a computer with data shown on screen">
                <button><a href="/response/questionnaires" name="View questionnaires looking for responses">View questionnaires looking for responses</a></button>
            </div>
            <div class="rightcontainer" style="float: left">
                <img src="/images/login.jpg" alt="image of a computer with data shown on screen and hands hovering over the keyboard">
                <button><a href="{{ route('register') }}">Sign up now to create your own questionnaires!</a></button>
            </div>
    </article>
</body>
</html>
