@extends('layouts.master')

@section('title', 'User Home')

@section('content')
    <h1>Home</h1>

    <div class="leftcontainer" style="float: left">
        <img src="/images/questionnaire.jpg" alt="image of a computer with data shown on screen">
        <button><a href="/response/questionnaires">View questionnaires looking for responeses</a></button>
    </div>
    <div class="rightcontainer" style="float: left">
        <img src="/images/view-questionnaires.jpg" alt="image of a computer with data shown on screen">
        <button><a href="/questionnaire" name="View your questionnaires">View your questionnaires</a></button>
    </div>

@endsection
