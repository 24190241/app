<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Thank You</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    <header>
        <nav class="navbar">
            <ul>
                <li class="left"><a href="/">Home</a></li>
                <li><a href="{{ route('login') }}">Log In</a></li>
            </ul>
        </nav>
    </header>
    <article class="thanks">
        <h1>Thank you for completing the questionnaire</h1>
        <div>
            <button><a href="/response/questionnaires">View more questionnaires looking for responses</a></button>
        </div>
        <div>
            <button><a href="/">Return to the home page</a></button>
        </div>
    </article>
</body>