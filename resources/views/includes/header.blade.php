<nav class="navbar">
    <ul>
        <li class="left"><a href="/home">Home</a></li>
        <li><a href="">Responses</a></li>
        <li><a href="/questionnaire">Questionnaires</a></li>
        <li><a href="{{ route('logout') }}"
            onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a></li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </ul>
</nav>
